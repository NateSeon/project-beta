# CarCar

Team: Alvi and NateAlvi and Nate

```
Alvi Khandakar - Service
Seungwon(Nate) Seon - Sales
```

## How to Run this Application:

git clone this: 
[gitlab](https://gitlab.com/alvi.khandakar/project-beta)
download Docker if you don't already have one
Do these commands:
```bash
    docker volume create beta-data
    docker-compose build
    docker-compose up
```
- All containers should be running in your docker and should be able to see all ports for this app.

```
check this url to see this application: http://localhost:3000/
```
[url](http://localhost:3000/)


## Application Diagram

![Screenshot of my project](project-beta diagram.png)

## Sales Microservice

- It basically consist of two directories; "api" and "poll" for backend
```
"api"
    In api directoy, there are three directories; "common", "sales_project", "sales_rest"
    "common"
        Inside this directory, there is json.py that has three encoders settled so that we could easily take advantage of it when to make our models and views
    "sales_project"
        important files you must know are "settings" and "urls"
            "settings"
                you install your apps and middleware here
                ('sales_rest.apps.SalesRestConfig', 'corsheaders', 'corsheaders.middleware.CorsMiddleware',)
                you give permission to certain ports 
            "urls"
                you put your admin urls and api urls here
    "sales_rest"
        important files you must know are "admin", "api_urls", api_views", "apps" and "models"
            "admin"
                you register your AutomobileVO here
            "api_urls"
                you put your microservices' urls here
                (api_sales_person_list, api_customer_list, api_sales_record_list, and api_sales_record_detail)
            "api_views"
                you put every encoders and view functions here
                (AutomobielVOEncoder, SalesPersonEncoder, CustomerEncoder, SalesRecordencoder, api_sales_person_list, api_customer_list, api_sales_record_list, and api_sales_record_detail)
            "apps"
                you put the exact same classname on your apps.py into your settings.py
            "models"
                you put every models for view functions here
                (AutomobileVO, SalesPerson, Customer, and SalesRecord)
"poll"
    there is "poller" file which has functions to poll 
    (get_automobiles, poll)
```
- For frontend, you need to check ghi.app.src
```
There are "App", "MainPage" and "Nav"
    "App" sets the paths of each microservices
    "MainPage" shows literally the main page of your service browser
    "Nav" sets the navigation bar on your web browser
There are "AutoForm", "CustomerForm", "ManufactuerForm", "ModelForm", "SalesPersonForm", and "SalesRecordForm"
    each componets shows the creation form
There are "AutoList", "CustomerList", "ManufacturerList", "ModelList", "SalesPersonList", and "SalesRecordList"
    each components shows the list 
```


## Services Microservice

- Also consists of two directories of api and polls for the backend
```
"api"
    In api directoy, there are three directories; "common", "service_project", "service_rest"
    "common"
        Inside this directory, there is json.py that has three encoders settled so that we could easily take advantage of it when to make our models and views
        The date encoder here is editted in order to separate date and time.
    "service_project"
        important files you must know are "settings" and "urls"
            "settings"
                you install your apps and middleware here; in comparison to the parent branch we added in
                ('service_rest.apps.ServiceRestConfig', 'corsheaders', 'corsheaders.middleware.CorsMiddleware',)
                you give permission to certain ports 
            "urls"
                you put your admin urls and api urls here
    "service_rest"
        important files you must know are "admin", "api_urls", api_views", "apps" and "models"
            "admin"
                All models are registered here, AutomobilesVO, Technicians, ServiceAppointment
            "api_urls"
                you put your microservices' urls here
                (api_list_service_appointments, api_show_appointment, api_list_technicians, api_show_tech, api_show_service_history)
            "api_views"
                you put every encoders and view functions here. The encoders show how the data will be inputted into the HTTP Request, and the view is the means at which that data is set up from the database.
                (AutomobileVOEncoder, TechnicianEncoder, ServiceAppointmentEncoder, HistoryEncoder)
                (api_list_service_appointments, api_show_appointment, api_list_technicians, api_show_tech, api_show_service_history)
            "apps"
                you put the exact same classname on your apps.py into your settings.py
            "models"
                you put every models for view functions here
                (AutomobileVO, Technician, SalesAppointment)
"poll"
    there is "poller" file which has functions to poll. This polls information from the inventory API located at the Inventory microservice at port 8100, and routes that information into AutomobileVO
    (get_auto(), poll())
```

- For frontend, you need to check ghi.app.src
```
There are "App", "MainPage" and "Nav"
    "App" sets the paths of each microservices
    "MainPage" shows literally the main page of your service browser
    "Nav" sets the navigation bar on your web browser
There are "AutoForm", "CustomerForm", "ManufactuerForm", "ModelForm", "NewTechicaianForm", and "ServiceForm"
    each componets shows the creation form
There are "AutoList", "CustomerList", "ManufacturerList", "ModelList", "ListServiceAppointments", and "TechnicianList", "ServiceHistory"
    each components shows the list 
```

## API Documentation
```
PORT 3000 - Connect to React APP
Port 15432 - Postgres DB
Port 8080 - Service API
Port 8090 - Sales API
Port 8100 - Inventory API
```

## Inventory API

 * Manufacturer API

|   Method      |       URL                                                                     |    What it does             |
| ------------- |:-----------------------------------------------------------------------------:| ---------------------------:|
| GET           | http://localhost:8100/api/manufacturers/                                      | GET List of Manufacturers   |
| GET           | http://localhost:8100/api/manufacturers/<int:pk>/                             | GET Manufacturer Details    |
| POST          | http://localhost:8100/api/manufacturers/                                      | POST Create Manufacturer    |
| PUT           | http://localhost:8100/api/manufacturers/<int:pk>/                             | PUT Update Manufacturer     |
| DELETE        | http://localhost:8100/api/manufacturers/<int:pk>/                             | DELETE Delete Manufacturer  |

 * Vehicle Model API

|   Method      |       URL                                                                     |    What it does              |
| ------------- |:-----------------------------------------------------------------------------:| ----------------------------:|
| GET           | http://localhost:8100/api/models/                                             | GET List of Vehicle Models   |
| GET           | http://localhost:8100/api/models/<int:pk>/                                    | GET Get Vehicle Model Details|
| POST          | http://localhost:8100/api/models/                                             | POST Create Vehicle Model    |
| PUT           | http://localhost:8100/api/models/<int:pk>/                                    | PUT Update Vehicle Model     |
| DELETE        | http://localhost:8100/api/models/<int:pk>/                                    | DELETE Delete Vehicle Model  |

 * Automobiles API

|   Method      |       URL                                                                     |    What it does           |
| ------------- |:-----------------------------------------------------------------------------:| --------------------------|
| GET           | http://localhost:8100/api/automobiles/                                        | GET List of Automobiles   |
| GET           | http://localhost:8100/api/automobiles/<str:vin>/                              | GET Get Automobile Details|
| POST          | http://localhost:8100/api/automobiles/                                        | POST Create Automobile    |
| PUT           | http://localhost:8100/api/automobiles/<str:vin>/                              | PUT Update Automobile     |
| DELETE        | http://localhost:8100/api/automobiles/<str:vin>/                              | DELETE Delete Automobile  |

![Inventory API Screenshot](Inventory API.png)


## Sales API

 * Salespeople API

|   Method      |       URL                                                                     |    What it does           |
| ------------- |:-----------------------------------------------------------------------------:| --------------------------|
| GET           | http://localhost:8090/api/salespeople/                                        | GET List of Salespeople   |
| POST          | http://localhost:8090/api/salespeople/                                        | POST Create Salesperson   |
| GET           | http://localhost:8090/api/salesrecords/                                       | GET List of Sales Records |
| POST          | http://localhost:8090/api/salesrecords/                                       | POST Create Sales Records |

 * Customers API

|   Method      |       URL                                                                     |    What it does           |
| ------------- |:-----------------------------------------------------------------------------:| --------------------------|
| GET           | http://localhost:8090/api/customers/                                          | GET Get List of Customers |
| POST          | http://localhost:8090/api/customers/                                          | POST Create Salesperson   |

![Sales API Screenshot](SALES API.png)



## Services API

 * Technicians

|   Method      |       URL                                                                     |    What it does           |
| ------------- |:-----------------------------------------------------------------------------:| --------------------------|
| GET           | http://localhost:8080/api/techicians /                                        | GET List of Technicians   |
| POST          | http://localhost:8080/api/technicians/                                        | POST Create a Technician  |
| GET           | http://localhost:8080/api/technicians/<int:pk>/                               | GET Details on Technician |
| PUT           | http://localhost:8080/api/technicians/<int:pk>/                               | PUT Update a Technician   |
| DELETE        | http://localhost:8080/api/technicians/<int:pk>/                               | DELETE Delete a Technician|

 * Service Appointments

|   Method      |       URL                                                                     |    What it does           |
| ------------- |:-----------------------------------------------------------------------------:| --------------------------|
| GET           | http://localhost:8080/api/appointments/                                       | GET List of Appointments  |
| POST          | http://localhost:8080/api/appointments/                                       | POST Create Appointment   |
| GET           | http://localhost:8080/api/appointments/<int:pk>/                              | GET Details on Appointment|
| PUT           | http://localhost:8080/api/appointments/<int:pk>/                              | PUT Update an Appointment |
| DELETE        | http://localhost:8080/api/appointments/<int:pk>/                              | DELETE Delete Appointment |
| GET           | http://localhost:8080/api/servicehistory/vin/<str:vin>/                       | GET Service History on Vin|

![Service API Screenshot](Service API.png)


## Value Objects
```
There is an "AutomobileVO" which created in sales poller. it has 'color', 'year', 'VIN', 'model', and 'import_href'.
Through this, you can poll from Inventory so that it could create with the microservices.
```
