import React, { useState } from 'react';

const SalesPersonForm = () => {

    const [salesPersonName, setName] = useState("");
    const [employeeNumber, setEmployeeNumber] = useState("");

    const handleSubmit = async (event) => {
        event.preventDefault();
        const newSalesPerson = {
            'name': salesPersonName,
            'employee_number': employeeNumber,
        }

        const salesPersonUrl = "http://localhost:8090/api/salespeople/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(newSalesPerson),
            headers: { 'Content-Type': 'application/json', },
        };

        fetch(salesPersonUrl, fetchConfig)
            .then(response => response.json())
            .then( () => {
                setName('');
                setEmployeeNumber('');
            })
            .catch(e => console.log("Error: ", e));
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleEmployeeNumberChange = (event) => {
        const value = event.target.value;
        setEmployeeNumber(value);
    }

    const handleClickBackChange = () => {
        window.location.assign('/');
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a SalesPerson</h1>
                    <form onSubmit={handleSubmit} id="create-bin-form">
                        <button onClick={handleClickBackChange} className="btn btn-dark">Back</button>
                        <p></p>
                        <div className="form-floating mb-3">
                            <input value={salesPersonName} onChange={handleNameChange} required type="text" name="salesperson name" id="salesperson_name" className="form-control" />
                            <label>Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={employeeNumber} onChange={handleEmployeeNumberChange} required type="number" name="employee number" id="employee_number" className="form-control" />
                            <label>Employee Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalesPersonForm;