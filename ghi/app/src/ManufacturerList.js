import React from "react"
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import './index.css';


function ManufacturerList(props) {
  const [manufacturers, setManufacturers] = useState([]);

  useEffect( () => {
    fetch("http://localhost:8100/api/manufacturers/")
      .then(response => response.json())
      .then(data => {setManufacturers (data.manufacturers);
      })
      .catch( e => console.log("Error: ", e));
  }, [])

	/* This section of Code is not used because we shouldn't allow Manufacturer Deletion... yet?
  const onDeleteAutoClick = (manufacturer) => {
    const manufacturerHref = manufacturer.href;
    const hrefComponents = manufacturer.href.split("/");
    const pk = hrefComponents[hrefComponents.length - 2];
    const manufacturersUrl = `http://localhost:8100/api/manufacturers/${pk}/`;
    const fetchConfig = {
      method: "DELETE",
      headers: {
        'Content-Type': 'application/json',
      },
    };
    fetch(manufacturersUrl, fetchConfig)
      .then(response => response.json())
      .then( data => {
        if (data.deleted) {
          const currentManufacturers = [...manufacturers];
          setManufacturers(currentManufacturers.filter( manufacturer => manufacturer.href !== manufacturerHref));
        }
      })
      .catch(e => console.log("Error: ", e));
  }
	*/


  return (
    <>
    <div><p></p></div>
    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
			<h1>Vehicle Manufacturers</h1>
      <Link to="/inventory/manufacturers/new">
      	<button className="btn btn-primary btn-lg px-4 gap-3">Add a Manufacturer</button>
			</Link>
    </div>
    <table className= "table table-striped">
      <thead>
        <tr>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        {manufacturers.map( manufacturer => {
          return (
            <tr key={manufacturer.href}> 
              <td>{manufacturer.name}</td>
            </tr>
          )
        })}
      </tbody>
    </table>
    </>
  )
}

export default ManufacturerList;