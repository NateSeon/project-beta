import React, { useEffect, useState } from 'react';

const ServiceForm = () => {

	const [technician, setTechNumber] = useState('');
    const [allTechs, setTechnician] = useState([])
	const [customer_name, setCustomerName] = useState('');
	const [vin, setVin] = useState('');
	const [reason, setReason] = useState('');
	const [date, setDate] = useState('');
	const [time, setTime] = useState('');

    useEffect( () => {
        fetch("http://localhost:8080/api/technicians/")
            .then(response => response.json())
            .then(data => {setTechnician (data.technicians);
            })
            .catch( e => console.log("Error: ", e));
            
        }, [])
        



	const handleSubmit = (event) => {
		event.preventDefault();
		const newService = {
			"technician": technician,
			"customer_name": customer_name,
			"vin": vin,
			"reason": reason,
			"date": date,
            "time": time,
		}

		const ServiceUrl = "http://localhost:8080/api/appointments/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(newService),
			headers: {
				'Content-Type': 'application/json',
			},
		}
		fetch(ServiceUrl, fetchConfig)
			.then(response => response.json())
			.then( () => {
				setTechNumber('');
				setCustomerName('');
				setVin('');
				setReason('');
				setDate('');
                setTime('');
			})
			.catch(e => console.log("Service Appointment Fetch Error: ", e))
	}

	const handleTechNumberChange = (event) => {
        const value = event.target.value;
        setTechNumber(value);
    }

    const handleCusomterNameChange = (event) => {
        const value = event.target.value;
        setCustomerName(value);
    }
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }
	const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }
    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
    }
    const handleTimeChange = (event) => {
        const value = event.target.value;
        setTime(value);
    }
    const handleClickBackChange = () => {
        window.location.assign('/');
    }

	return (
        <div className="my-5 container">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a New Service Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-model-form">
                        <button onClick={handleClickBackChange} className="btn btn-dark">Back</button>
                        <p></p>
                        <div className="form-floating mb-3">
                            <input value={vin} onChange={handleVinChange} required type="text" name="vin" id="vin" className="form-control" />
                            <label htmlFor="vin">Vehicle VIN Number</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={customer_name} onChange={handleCusomterNameChange} required type="text" name="customer_name" id="customer_name" className="form-control" />
                            <label htmlFor="customer_name">Customer Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleTechNumberChange} value={technician} required name = "technician" id="technician" className="form-select">
                            <option value="">Choose a Technician</option>
                                {allTechs.map( tech => {
                                    return (
                                        <option key={tech.employee_number} value={tech.employee_number}>
                                        {tech.tech_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={reason} onChange={handleReasonChange} required type="text" name="reason" id="reason" className="form-control" />
                            <label htmlFor="reason">Reason</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <div>
                                <label htmlFor="appointment">Date of Appointment:</label>
                            </div>
                            <input type="date" id="appointment" name="appointment-date" 
                                onChange={handleDateChange} className="form-control" value={date}></input>
                        </div>
                        <div className='form-floating mb-3'>
                            <div>
                                <label htmlFor="appointment">Time of Appointment:</label>
                            </div>
                            <input type="time" id="timeAppt" name="appointment-time"
                                onChange={handleTimeChange} className="form-control"value={time}></input>
                        </div>
                        <p></p>
                        <div>
                        <button className="btn btn-primary">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ServiceForm;
