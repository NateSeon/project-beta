import React, { useEffect, useState } from "react"


const SalesRecordForm = () => {

    const [price, setPrice] = useState('');
    const [salesPerson, setSalesPerson] = useState('');
    const [customer, setCustomer] = useState('');
    const [automobile, setAutomobile] = useState('');
    const [salesPeople, setSalesPeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);

    useEffect(() => {
    const salesPeopleVOsUrl = "http://localhost:8090/api/salespeople/";
    fetch(salesPeopleVOsUrl)
            .then(response => response.json())
            .then(data => setSalesPeople(data.sales_people))
            .catch((e) => console.error("Error: ", e));
    }, []);

    useEffect(() => {
    const customerVOsUrl = "http://localhost:8090/api/customers/";
    fetch(customerVOsUrl)
            .then(response => response.json())
            .then(data => setCustomers(data.customers))
            .catch((e) => console.error("Error: ", e));
    }, []);

    useEffect(() => {
        const automobilVOsUrl = "http://localhost:8100/api/automobiles/";
    fetch(automobilVOsUrl)
        .then(response => response.json())
        .then(data => setAutomobiles(data.autos))
        .catch((e) => console.error("Error: ", e));
    }, []);

    const handleSubmit = (event) => {
        event.preventDefault();
        const newSalesRecord = {
            'price': price,
            'customer': customer,
            'sales_person': salesPerson,
            'automobile': automobile,
        }

        const salesRecordUrl = "http://localhost:8090/api/salesrecords/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(newSalesRecord),
            headers: { 'Content-Type': 'application/json', },
        };
        console.log(newSalesRecord);

        fetch(salesRecordUrl, fetchConfig)
            .then(response => response.json())
            .then( () => {
                setPrice('');
                setSalesPerson('');
                setCustomer('');
                setAutomobile('');
            })
            .catch(e => console.log("Error: ", e));
    }

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }
    const handleSalesPersonChange = (event) => {
        const value = event.target.value;
        setSalesPerson(value);
    }
    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }
    const handleClickBackChange = () => {
        window.location.assign('/');
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Sales Record</h1>
                    <form onSubmit={handleSubmit} id="create-bin-form">
                        <button onClick={handleClickBackChange} className="btn btn-dark">Back</button>
                        <p></p>
                        <div className="form-floating mb-3">
                            <input value={price} onChange={handlePriceChange} required type="number" name="price" id="price" className="form-control" />
                            <label>Price</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleCustomerChange} value={customer} required id="customer" name="customer" className="form-select">
                                <option value="">Choose a Customer</option>
                                {customers.map(customer => {
                                    return (
                                        <option key={customer.id} value={customer.phone}>{customer.name}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleSalesPersonChange} value={salesPerson} required id="salesPerson" name="salesPerson" className="form-select">
                                <option value="">Choose a Sales Person</option>
                                {salesPeople.map(salesPerson => {
                                    return (
                                        <option key={salesPerson.id} value={salesPerson.employee_number}>{salesPerson.name}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleAutomobileChange} value={automobile} required id="automobile" name="automobile" className="form-select">
                                <option value="">Choose a VIN</option>
                                {automobiles.map(automobile => {
                                    return (
                                        <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalesRecordForm;