import React, { useEffect, useState } from 'react';

const AutoForm = () => {

	const [autoColor, setAutoColor] = useState('');
	const [autoYear, setAutoYear] = useState('');
	const [autoVin, setAutoVin] = useState('');
	const [autoModel, setAutoModel] = useState('');
	const [models, setModels] = useState([]);


	useEffect( () => {
	const modelVOsUrl = 'http://localhost:8100/api/models/';
	fetch(modelVOsUrl)
		.then(response => response.json())
		.then(data => setModels(data.models))
		.catch(e => console.error('Error: ', e))
	}, [])

	
	const handleSubmit = (event) => {
		event.preventDefault();
		const newAuto = {
			'color': autoColor,
			'year': autoYear,
			'vin': autoVin,
			'model_id': autoModel,
		}

		const autoUrl = "http://localhost:8100/api/automobiles/";
		const fetchConfig = {
			method: "POST",
			body: JSON.stringify(newAuto),
			headers : {'Content-Type': 'application/json',},
			};
		fetch(autoUrl, fetchConfig)
			.then(response => response.json())
			.then( () => {
				setAutoColor('');
				setAutoYear('');
				setAutoVin('');
				setAutoModel('');
			})
			.catch(e => console.log("Error: ", e));
	}


	const handleColorChange = (event) => {
		const value = event.target.value;
		setAutoColor(value);
	}

	const handleYearChange = (event) => {
		const value = event.target.value;
		setAutoYear(value);
	}
	const handleVinChange = (event) => {
		const value = event.target.value;
		setAutoVin(value);
	}
	const handleModelChange = (event) => {
		const value = event.target.value;
		setAutoModel(value);
	}
	const handleClickBackChange = () => {
		window.location.assign('/');
	}

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add an Automobile to Your Inventory</h1>
                    <form onSubmit={handleSubmit} id="create-bin-form">
						<button onClick={handleClickBackChange} className="btn btn-dark">Back</button>
						<p></p>
                        <div className="form-floating mb-3">
                            <input value={autoColor} onChange={handleColorChange} required type="text" name="color" id="color" className="form-control" />
                            <label>Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={autoYear} onChange={handleYearChange} required type="text" name="year" id="year" className="form-control" />
                            <label>Year</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={autoVin} onChange={handleVinChange} required type="text" name="vin" id="vin" className="form-control" />
                            <label>VIN</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleModelChange} value={autoModel} required id="model" name="model" className="form-select">
                                <option value="">Choose a Model</option>
                                {models.map(model => {
                                    return (
                                        <option key={model.id} value={model.id}>{model.name}</option>
                                    );
                                })}
                            </select>
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default AutoForm;