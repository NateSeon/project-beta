import React, { useState } from 'react';

const ManufacturerForm = () => {

	const [manufacturerName, setManufacturerName] = useState('');
	
	const handleSubmit = (event) => {
		event.preventDefault();
		const newManufacturer = {
			'name': manufacturerName,
		}

		const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
		const fetchConfig = {
			method: "POST",
			body: JSON.stringify(newManufacturer),
			headers : {'Content-Type': 'application/json',},
			};
		fetch(manufacturerUrl, fetchConfig)
			.then(response => response.json())
			.then( () => {
				setManufacturerName('');
			})
			.catch(e => console.log("Error: ", e));
	}


	const handleManufacturerChange = (event) => {
		const value = event.target.value;
		setManufacturerName(value);
	}
	const handleClickBackChange = () => {
		window.location.assign('/');
	}


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-bin-form">
						<button onClick={handleClickBackChange} className="btn btn-dark">Back</button>
						<p></p>
                        <div className="form-floating mb-3">
                            <input value={manufacturerName} onChange={handleManufacturerChange} required type="text" name="manufacturer name" id="manufacturer_name" className="form-control" />
                            <label>Name</label>
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ManufacturerForm;
